Есть ряд улучшений, который можно сделать: (разбито на 2 части)
------------------------
1. Можно было использовать php8.0 и mysql8

2. Можно добавить типы к свойствам классов

3. Использование extends AbstractController лучше избегать. Использование методов из AbstractController работает как service locator. DI предпочтительнее.
https://gitlab.com/4klaasa/symfony_api/-/blob/master/src/Controller/Api/ArticlesController.php#L23

4. Тут (и в остальных местах) лучше сделать ArticleRepository и в DI его.
https://gitlab.com/4klaasa/symfony_api/-/blob/master/src/Controller/Api/ArticlesController.php#L28
5. Если список пустой то просто отдаем пустой массив.
https://gitlab.com/4klaasa/symfony_api/-/blob/master/src/Controller/Api/ArticlesController.php#L29

6. Можно и так, но лучше создавать responce в контроллере и использовать сериалайзер также руками.
https://gitlab.com/4klaasa/symfony_api/-/blob/master/src/Controller/Api/ArticlesController.php#L32

7. Тут и в остальных местах, можно использовать https://wiki.php.net/rfc/constructor_promotion. Ну на php8 конечно.
https://gitlab.com/4klaasa/symfony_api/-/blob/master/src/Service/Article/CreateArticle.php#L33

8. flush() лучше использовать как можно ближе к контроллеру. Чтобы лучше исполдьзовать идею UnitOfWork
https://gitlab.com/4klaasa/symfony_api/-/blob/master/src/Service/Article/CreateArticle.php#L53

9. Сериалайзер может десериализовать в существующий объект. (https://symfony.com/doc/current/components/serializer.html#deserializing-in-an-existing-object)
https://gitlab.com/4klaasa/symfony_api/-/blob/master/src/Service/Article/UpdateArticle.php#L56

10. Сериалайзер так же может десериализовать связанные сущности. Но скорее всего может понадобится кастомный денормалайзер
https://gitlab.com/4klaasa/symfony_api/-/blob/master/src/Service/Article/UpdateArticle.php#L65

11. Если $tagsIds - будет пустым, то наверно надо пустоту отдавать (но это зависит от БЛ)
https://gitlab.com/4klaasa/symfony_api/-/blob/master/src/Service/Article/FindArticles.php#L42

12. Если не нашли то пустой список надо возвращать. Сервис не должен знать про инфраструктурный уровень. Что надо 404 отдавать. Его ведь можно и из консоли вызвать.
https://gitlab.com/4klaasa/symfony_api/-/blob/master/src/Service/Article/FindArticles.php#L51

src/Serializer/EntityNormalizer.php#L14
13. Как раз что бы не городито такое, можно использовать сериалайзер напрямую в контроллерах и использовать группы сериализации.

14. Сущность в любой момент времени должна быть валидна с т.з. инфрастурктуры. (обязательные поля)
Это означает что все свойства либо должны иметь дефолт значения, либо принимать обязательные параметры в конструкторе.
https://gitlab.com/4klaasa/symfony_api/-/blob/master/src/Entity/Article.php - надо запрашивать в конструкторе, без title сущность не имеет смысла.
https://gitlab.com/4klaasa/symfony_api/-/blob/master/src/Entity/Article.php#L26 - может быть null
https://gitlab.com/4klaasa/symfony_api/-/blob/master/src/Entity/Article.php#L65 - лишний сеттор (врятли есть бизнес логика где мы будем менять id у статьи)
https://gitlab.com/4klaasa/symfony_api/-/blob/master/src/Entity/Article.php#L88 - ретурн this у сетторов лучше не использовать (https://ocramius.github.io/blog/fluent-interfaces-are-evil/)
https://gitlab.com/4klaasa/symfony_api/-/blob/master/src/Entity/Article.php#L106 - тут нужна валидация, т.е. в массиве может прийти что угодно.
https://gitlab.com/4klaasa/symfony_api/-/blob/master/src/Entity/Article.php#L106 - иногда нужны сетеры коллекций, но часто более убодно делать add/remove методы для элементов. addTag(Tag $tag)|removeTag(Tag $tag)

------ Часть 2 -------

https://gitlab.com/4klaasa/symfony_api/-/blob/master/migrations/Version20210720181000.php#L23
лучше использовать UNSIGNED для целочисленных колонок, в которых не планируется хранить отрицательные значения

https://gitlab.com/4klaasa/symfony_api/-/blob/master/src/Controller/Api/ArticlesController.php#L28
название переменной $all стоит пересмотреть
getRepository - отказаться от использования service locator
нужен репозиторий для работы с App\Entity\Article

https://gitlab.com/4klaasa/symfony_api/-/blob/master/src/Controller/Api/ArticlesController.php#L29
empty тут можно не использовать и нужно не использовать. В целом нужно изберать использования функций empty и isset, в пользу более конкретных проверок

https://gitlab.com/4klaasa/symfony_api/-/blob/master/src/Controller/Api/ArticlesController.php#L30
использование двойных кавычек не по назначению

https://gitlab.com/4klaasa/symfony_api/-/blob/master/src/Service/Article/CreateArticle.php#L2
declare(strict_types = 1); не указан здесь и в других классах. Лучше использовать

https://gitlab.com/4klaasa/symfony_api/-/blob/master/src/Service/Article/CreateArticle.php#L53
flush() лучше делать в контроллере

https://gitlab.com/4klaasa/symfony_api/-/blob/master/src/Service/Article/CreateArticle.php#L44
здесь в использовании магии нет необходимости, лучше сделать метод с понятным названием

замечания по App\Service\Article\CreateArticle также относятся к
App\Service\Article\UpdateArticle
App\Service\Article\DeleteArticle
App\Service\Article\FindArticles
App\Service\Tag\CreateTag
App\Service\Tag\UpdateTag

https://gitlab.com/4klaasa/symfony_api/-/blob/master/src/Controller/Api/ArticlesController.php#L37
нужно стремиться использовать более говорящие названия для переменных

https://gitlab.com/4klaasa/symfony_api/-/blob/master/src/Controller/Api/ArticlesController.php#L71
поиск по идентификатору, поэтому можно использовать метод find($id)

https://gitlab.com/4klaasa/symfony_api/-/blob/master/src/Controller/Api/ArticlesController.php#L83
нужно использовать $request->query->all('tags_ids'). get для получения массива в симфони - деприкейтед

https://gitlab.com/4klaasa/symfony_api/-/blob/master/src/Controller/Api/TagsController.php#L27
текстовое сообщение с пояснением можно указать
в идеале можно реализовывать свои классы исключений и отказаться от использования инфраструктурных

https://gitlab.com/4klaasa/symfony_api/-/blob/master/src/Service/Article/FindArticles.php#L42
GROUP BY HAVING здесь лишнее или используется неправильно
