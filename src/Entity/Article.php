<?php

declare(strict_types = 1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Tag
 * @package App\Entity
 * @ORM\Table(name="app_articles")
 * @ORM\Entity()
 */
class Article
{

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100)
     */
    private $title;

    /**
     * @var Tag[]
     *
     * @ORM\ManyToMany(targetEntity="Tag",cascade={"persist"})
     * @ORM\JoinTable(
     *     name="article_tags",
     *     joinColumns={@ORM\JoinColumn(name="article_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="tag_id", referencedColumnName="id")}
     * )
     */
    private $tags;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return Article
     */
    public function setId(int $id) : Article
    {
        $this->id = $id;
        return $this;
    }


    /**
     * @return string
     */
    public function getTitle() : string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return Article
     */
    public function setTitle(string $title) : Article
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return array
     */
    public function getTags() : array
    {
        return $this->tags->toArray();
    }

    /**
     * @param array $tags
     *
     * @return $this
     */
    public function setTags(array $tags) : Article
    {
        $this->tags = new ArrayCollection($tags);
        return $this;
    }


}