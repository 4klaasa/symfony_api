<?php


namespace App\Service\Tag;


use App\Entity\Tag;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class UpdateArticle
 * @package App\Service\Article
 */
class UpdateTag
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * UpdateTag constructor.
     *
     * @param EntityManagerInterface $em
     * @param SerializerInterface    $serializer
     */
    public function __construct(
        EntityManagerInterface $em,
        SerializerInterface $serializer
    ) {
        $this->em         = $em;
        $this->serializer = $serializer;
    }

    /**
     * @param int    $id
     * @param string $json
     *
     * @return Tag
     * @throws ORMException
     */
    public function __invoke(int $id, string $json) : Tag
    {
        /** @var Tag $tag */
        $tag = $this->em->getReference(Tag::class, $id);

        /** @var Tag $tag_new */
        $tag_new = $this->serializer->deserialize($json, Tag::class, 'json');

        $tag->setName($tag_new->getName());

        $this->em->persist($tag);
        $this->em->flush();

        return $tag;
    }
}
