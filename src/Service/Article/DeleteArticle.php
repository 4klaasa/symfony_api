<?php
declare(strict_types = 1);

namespace App\Service\Article;


use App\Entity\Article;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;

/**
 * Class DeleteArticle
 * @package App\Service\Article
 */
class DeleteArticle
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * DeleteArticle constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @throws ORMException
     */
    public function __invoke(int $id) : void
    {
        $article = $this->em->getReference(Article::class, $id);
        $this->em->remove($article);
        $this->em->flush();
    }
}