<?php


namespace App\Service\Article;


use App\Entity\Article;
use App\Entity\Tag;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class UpdateArticle
 * @package App\Service\Article
 */
class UpdateArticle
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * CreateArticle constructor.
     *
     * @param EntityManagerInterface $em
     * @param SerializerInterface    $serializer
     */
    public function __construct(
        EntityManagerInterface $em,
        SerializerInterface $serializer
    ) {
        $this->em         = $em;
        $this->serializer = $serializer;
    }

    /**
     * @param int    $id
     * @param string $json
     *
     * @return Article
     * @throws ORMException
     */
    public function __invoke(int $id, string $json) : Article
    {
        /** @var Article $article */
        $article = $this->em->getReference(Article::class, $id);

        /** @var Article $article_new */
        $article_new = $this->serializer->deserialize($json, Article::class, 'json');

        $tags = array_map(
            function ($item) {
                return $this->em->getReference(Tag::class, $item->getId());
            },
            $article_new->getTags()
        );

        $article->setTags($tags);
        $article->setTitle($article_new->getTitle());

        $this->em->persist($article);
        $this->em->flush();

        return $article;
    }
}
