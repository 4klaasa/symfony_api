<?php


namespace App\Service\Article;


use App\Entity\Article;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class CreateArticle
 * @package App\Service\Article
 */
class CreateArticle
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * CreateArticle constructor.
     *
     * @param EntityManagerInterface $em
     * @param SerializerInterface    $serializer
     */
    public function __construct(EntityManagerInterface $em, SerializerInterface $serializer)
    {
        $this->em         = $em;
        $this->serializer = $serializer;
    }

    /**
     * @param string $json
     *
     * @return Article
     */
    public function __invoke(string $json) : Article
    {
        $article = $this->serializer->deserialize(
            $json,
            Article::class,
            'json'
        );

        $this->em->persist($article);
        $this->em->flush();

        return $article;
    }
}