<?php


namespace App\Service\Article;


use App\Entity\Article;
use Doctrine\DBAL\Exception\DatabaseObjectNotFoundException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Serializer\SerializerInterface;

class FindArticles
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * FindArticles constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em         = $em;
    }

    /**
     * @param int[] $tagsIds
     *
     * @return Article[]
     */
    public function __invoke(array $tagsIds) : array
    {
        if (empty($tagsIds)) {
            return $this->em->getRepository(Article::class)->findAll();
        }

        $query = $this->em->createQuery('SELECT a FROM App\Entity\Article a JOIN a.tags t WHERE t.id IN (:tags) GROUP BY a.id HAVING count(t.id) = :count');
        $query->setParameters([
            'tags'  => $tagsIds,
            'count' => count($tagsIds)
        ]);

        $result = $query->getResult();

        if (empty($result)) {
            throw new NotFoundHttpException("Not found");
        }

        return $result;
    }
}