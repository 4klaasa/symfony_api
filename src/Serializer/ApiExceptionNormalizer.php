<?php

namespace App\Serializer;

use Psr\Log\LoggerInterface;
use Symfony\Component\ErrorHandler\Exception\FlattenException;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Class ApiExceptionNormalizer
 * @package App\Serializer
 */
class ApiExceptionNormalizer implements NormalizerInterface
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * ApiExceptionNormalizer constructor.
     *
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param mixed       $exception
     * @param string|null $format
     * @param array       $context
     *
     * @return array
     */
    public function normalize($exception, string $format = null, array $context = [])
    {
        $this->logger->error($exception->getMessage());

        /** @var $exception FlattenException */
        return [
            'exception' => [
                'message' => $exception->getMessage(),
                'code'    => $exception->getStatusCode(),
                'trace'   => $exception->getTraceAsString(),
            ],
        ];
    }

    /**
     * @param mixed       $data
     * @param string|null $format
     *
     * @return bool
     */
    public function supportsNormalization($data, string $format = null)
    {
        return $data instanceof FlattenException;
    }
}
