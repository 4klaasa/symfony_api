<?php

declare(strict_types = 1);

namespace App\Controller\Api;

use App\Entity\Article;
use App\Service\Article\CreateArticle;
use App\Service\Article\DeleteArticle;
use App\Service\Article\FindArticles;
use App\Service\Article\UpdateArticle;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * Class TagController
 * @package App\Controller
 */
class ArticlesController extends AbstractController
{

    public function list() : JsonResponse
    {
        $all = $this->getDoctrine()->getRepository(Article::class)->findAll();
        if (empty($all)) {
            throw new NotFoundHttpException("Not found");
        }
        return $this->json($all);
    }

    public function create(
        Request $request,
        CreateArticle $service
    ) : JsonResponse {
        return $this->json(
            $service($request->getContent())
        );
    }

    /**
     * @throws ORMException
     */
    public function edit(
        int $id,
        Request $request,
        UpdateArticle $service
    ) : JsonResponse {
        return $this->json(
            $service($id, $request->getContent())
        );
    }

    /**
     * @throws ORMException
     */
    public function delete(
        int $id,
        DeleteArticle $service
    ) : JsonResponse {
        $service($id);
        return $this->json(true);
    }

    public function findOne(
        int $id
    ) : JsonResponse {
        $article = $this->getDoctrine()->getRepository(Article::class)->findOneBy(['id' => $id]);
        if (empty($article)) {
            throw new NotFoundHttpException("Not found");
        }
        return $this->json($article);
    }

    public function find(
        Request $request,
        FindArticles $service
    ) : JsonResponse {
        return $this->json(
            $service((array) $request->query->get('tags_ids', []))
        );
    }

}