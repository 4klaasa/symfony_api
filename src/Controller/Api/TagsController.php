<?php

declare(strict_types = 1);

namespace App\Controller\Api;

use App\Entity\Tag;
use App\Service\Tag\CreateTag;
use App\Service\Tag\UpdateTag;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class TagsController
 * @package App\Controller\Api
 */
class TagsController extends AbstractController
{

    public function list() : JsonResponse
    {
        $all = $this->getDoctrine()->getRepository(Tag::class)->findAll();
        if (empty($all)) {
            throw new NotFoundHttpException();
        }
        return $this->json($all);
    }

    public function create(
        Request $request,
        CreateTag $service
    ) : JsonResponse {
        return $this->json(
            $service($request->getContent())
        );
    }

    /**
     * @throws ORMException
     */
    public function edit(
        int $id,
        Request $request,
        UpdateTag $service
    ) : JsonResponse {
        return $this->json(
            $service($id, $request->getContent())
        );
    }

}